import json
import urllib2

# frontend API service that takes a chat message string as input and returns a JSON object
def frontEndService(str):
	# split the string to different parts by space
	parts = str.split()

	# data is what will be used to construct the json object
	data = {}

	# lists of mentions and links
	mentionList = []
	linksList = []

	# loop through each part and check if a part has a mention or a link
	for part in parts:
		if '@' in part:
			mentionList.append(part[1:])
		if 'http' in part:
			backEndService(part, linksList)

	# build data if the lists are not empty
	if mentionList:
		data["mentions"] = mentionList
	if linksList:
		data["links"] = linksList

	# create the json object from data
	jason_data = json.dumps(data, indent=2, separators=(',', ': '))
	return jason_data

# helper method to get the HTML from a URL
def get_HTML(URL):
	html = None
	try:
		# use urllib2 to open the URL
		response = urllib2.urlopen(URL)
		html = response.read()
	except Exception, e:
		print "Error occured when getting the HTML:", e

	return html

# backend service that processes the links and gets the URLs and the page's title
def backEndService(URL, linksList):
	linksDict={}
	linksDict["url"] = URL
	tag_name = ['<title>', '</title>']

	if get_HTML(URL) is not None:
		html = get_HTML(URL)

		# find the start and end indices of the content inside the "title" tag
		titleStartIndex = html.find(tag_name[0]) + len(tag_name[0])
		titleEndIndex = html.find(tag_name[1])
		title = html[titleStartIndex:titleEndIndex]
		linksDict["title"] = title
		linksList.append(linksDict)

# main function with test cases
def main():
	test_cases = ["@chris you around?", 
				  "Olympics are starting soon; http://www.nbcolympics.com",
				  "@bob @john is such a cool feature; https://twitter.com/jdorfman/status/430511497475670016",
				  "@tom @paul this test has 2 links and 2 mentions http://www.nbcolympics.com https://twitter.com/jdorfman/status/430511497475670016",
				  "@peter this test has an invalid url https://www.invalidurl.com/",
				  "@mary this test contains a URL with a quotation https://twitter.com/freeCodeCamp/status/896868105917485061",
				  "@mary & @alexa @ - This is a mixed test! Let's see this link https://twitter.com/freeCodeCamp/status/896868105917485061"]

	# print the result of each test case			  
	for test_case in test_cases:
		print('*****************************************************')
		print(frontEndService(test_case))

if __name__ == '__main__':
	main()
